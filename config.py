import os

SCRIPT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))
KEY_POLLINGMODE = "polling_mode"
KEY_SW_GEOMETRY = "statusWindowGeometry"
KEY_SW_STATE = "statusWindowStatus"