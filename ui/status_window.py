import os
from PySide import QtGui
from PySide.QtCore import Slot, QEvent, Qt, Signal
from PySide.QtGui import QMainWindow, QMenu, QSystemTrayIcon, QAction, QIcon
from ui.ui_utils import load_ui
import config


class StatusWindow(QMainWindow):
    def __init__(self, parent=None, proxyManager=None, settings=None):
        QMainWindow.__init__(self, parent)
        # restore window geometry
        self.settings = settings
        self.proxyManager = proxyManager
        self.restoreGeometry(settings.value(config.KEY_SW_GEOMETRY))
        # remove default buttons and frames, adding title frame
        self.setWindowFlags(Qt.CustomizeWindowHint | Qt.WindowTitleHint)
        # create tray menu
        self.trayIconMenu = QMenu(self)
        self.trayIconMenu.addSeparator()
        self.trayIconMenu.addAction(QAction("&Quit", self, triggered=QtGui.qApp.quit))
        # preload tray icon
        self.trayIcon = QSystemTrayIcon(self)
        # setting object name, needed by connectSlotsByName
        self.trayIcon.setObjectName("trayicon")
        self.trayIcon.setContextMenu(self.trayIconMenu)
        self.trayIcon.setIcon(QIcon('resources/icon_32x32.png'))
        self.trayIcon.show()
        # apply ui based window load to the current window
        load_ui(os.path.join(config.SCRIPT_DIRECTORY, 'ui/status_window.ui'), self)
        # restore window state
        self.restoreState(settings.value(config.KEY_SW_STATE))

    # auto bind slot for ui button named pushButton
    @Slot()
    def on_pollingButton_clicked(self):
        self.settings.setValue(config.KEY_POLLINGMODE, True)
        self.proxyManager.activate_polling_mode()

    @Slot()
    def on_dbusButton_clicked(self):
        self.settings.setValue(config.KEY_POLLINGMODE, False)
        self.proxyManager.activate_dbus_mode()

    # auto bind slot for tray icon activation
    @Slot(QSystemTrayIcon.ActivationReason)
    def on_trayicon_activated(self, reason):
        if reason in (QSystemTrayIcon.Trigger, QSystemTrayIcon.DoubleClick):
            if self.isVisible():
                self.hide()
            else:
                self.show()
        elif reason == QSystemTrayIcon.MiddleClick:
            print('middle')

    # override of change event listener to force minimize to sys tray
    def changeEvent(self, event):
        if event.type() == QEvent.WindowStateChange:
            if self.windowState() & Qt.WindowMinimized:
                event.ignore()
                self.close()
                return
        super(StatusWindow, self).changeEvent(event)

    # override of close event listener to force minimize to sys tray
    def closeEvent(self, event):
        event.ignore()
        self.hide()
        self.trayIcon.showMessage('Running', 'Running in the background.')