#!/usr/bin/python3
# GLOBAL IMPORTS
import sys
import os
import logging
import logging.config
from PySide.QtCore import QSettings
from PySide.QtGui import QApplication
# LOCAL IMPORTS
from proxy.proxy_manager import ProxyManager
from ui.status_window import StatusWindow
import config


def pre_close_operations():
    # saving geometry and status of statusWindow before closing the app
    settings.setValue(config.KEY_SW_GEOMETRY, statusWin.saveGeometry())
    settings.setValue(config.KEY_SW_STATE, statusWin.saveState())
    settings.setValue(config.KEY_POLLINGMODE, proxyManager.pollingMode)
    # stopping manager and waiting for completion
    proxyManager.stop()
    proxyManager.wait()

if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger()

    app = QApplication(sys.argv)
    # binding operations to be executed before closing the application
    app.aboutToQuit.connect(pre_close_operations)
    # setting app info necessary for settings object binding
    app.setOrganizationName("Qantic")
    app.setOrganizationDomain("ntrp.qantic.org")
    app.setApplicationName("ProxyManager")
    settings = QSettings()
    # starting proxy manager
    logger.info("Starting the proxy manager")
    proxyManager = ProxyManager(settings=settings)
    proxyManager.start()
    # creating status hidden window and passing settings to it
    statusWin = StatusWindow(proxyManager=proxyManager, settings=settings)
    sys.exit(app.exec_())