import dbus
import logging
import time
from pprint import pprint
import ipaddress
from PySide.QtCore import QThread, QMutex, QWaitCondition, QMutexLocker, Signal
from dbus.mainloop.glib import DBusGMainLoop
import config
from proxy.if_utils import getifaces


class Sync:
    netChanged = QWaitCondition()  # net processor thread wait condition
    delayPassed = QWaitCondition()  # polling thread wait condition
    mutex = QMutex()  # net processor and signal listener sync mutex
    delayMutex = QMutex()  # polling thread mutex for wait condition
    reschedule = False


class PollingThread(QThread):
    """
    Thread responsible for net polling if DBUS mode is not active
    """

    def __init__(self):
        QThread.__init__(self)
        self.logger = logging.getLogger(__name__)
        self.doPoll = True
        self.ifaces = None

    def run(self):

        while self.doPoll:
            # wait until the timeout and re-poll, can be waken up by application closing
            ifaces = getifaces(True).keys()
            if self.ifaces != ifaces:
                if self.ifaces:
                    self.logger.info('Network change detected')
                    if not Sync.mutex.tryLock():
                        self.logger.warn('Proxy reconfigure is already running, Rescheduling!')
                        Sync.reschedule = True
                    Sync.netChanged.wakeAll()
                    Sync.mutex.unlock()
                self.ifaces = ifaces
            Sync.delayPassed.wait(Sync.delayMutex, 3000)
        self.logger.info('Polling thread stopped')

    # graceful stop method
    def stop(self):
        self.doPoll = False


class ProxyManager(QThread):
    """
    NM_STATE_UNKNOWN = 0
    NM_STATE_ASLEEP = 10
    NM_STATE_DISCONNECTED = 20
    NM_STATE_DISCONNECTING = 30
    NM_STATE_CONNECTING = 40
    NM_STATE_CONNECTED_LOCAL = 50
    NM_STATE_CONNECTED_SITE = 60
    NM_STATE_CONNECTED_GLOBAL = 70

    Thread responsible for proxy reconfiguration, can be waken by polling or dbus signal
    """

    def __init__(self, settings=None):
        self.logger = logging.getLogger(__name__)
        QThread.__init__(self)
        self.settings = settings
        self.doRun = True
        self.reschedule = False
        self.bus = None
        self.pollingThread = None
        self.pollingMode = self.settings.value(config.KEY_POLLINGMODE, False) in ['true', 'True', 'TRUE', '1']
        if self.pollingMode:
            self.activate_polling_mode(True)
        else:
            self.activate_dbus_mode(True)

    def activate_polling_mode(self, force=False):
        if self.bus:
            self.logger.info('Removing DBUS signal receiver')
            self.bus.remove_signal_receiver(self.on_dbus_notification,
                                            dbus_interface='org.freedesktop.NetworkManager',
                                            signal_name='PropertiesChanged')
            self.bus = None
        if not self.pollingMode or force:
            self.logger.info('Initializing net status polling thread')
            self.pollingThread = PollingThread()
            self.pollingThread.start()
            self.logger.info('Polling mode activated')
            self.pollingMode = True

    def activate_dbus_mode(self, force=False):
        if self.pollingThread and self.pollingThread.isRunning():
            self.pollingThread.stop()
            Sync.delayPassed.wakeAll()
            self.pollingThread.wait()
            self.pollingThread = None
        if self.pollingMode or force:
            self.logger.info('Initializing DBUS signal receiver')
            dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
            self.bus = dbus.SystemBus()
            self.bus.add_signal_receiver(self.on_dbus_notification,
                                         dbus_interface='org.freedesktop.NetworkManager',
                                         signal_name='PropertiesChanged')
            self.logger.info('DBUS mode activated')
            self.pollingMode = False

    def run(self):
        self.logger.info('ProxyManager started')
        while self.doRun:
            Sync.mutex.lock()
            if not Sync.reschedule:
                self.logger.info('Waiting for wake notify')
                Sync.netChanged.wait(Sync.mutex)
            else:
                self.logger.info('Reschedule detected')
            Sync.reschedule = False
            if self.doRun:
                self.process_net_change()
            Sync.mutex.unlock()
        self.logger.info('ProxyManager  Stopped')

    # main processor
    def process_net_change(self):
        self.logger.info('Proxy reconfigure started')
        for i in range(5):
            print('.')
            time.sleep(1)
        self.logger.info('Proxy reconfigure completed')

    # dbus mode, waiting for signal
    def on_dbus_notification(self, result):
        state = result.get('State', -1)
        if state > 40 or state == 20:
            self.logger.info('Network change signal received')
            if not Sync.mutex.tryLock():
                self.logger.warn('Network changed while resetting proxy, Rescheduling!')
                Sync.reschedule = True
            Sync.netChanged.wakeAll()
            Sync.mutex.unlock()

    def stop(self):
        self.doRun = False
        if self.pollingMode:
            self.pollingThread.stop()
            Sync.delayPassed.wakeAll()
        Sync.netChanged.wakeAll()